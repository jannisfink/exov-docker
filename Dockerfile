FROM alpine

WORKDIR /var/opt/exov
COPY . .

RUN ln -s /var/opt/exov/exov /bin/exov

EXPOSE 8080

CMD [ "exov" ]
